"""
Module for loading .psl data
information we need to keep 
-> ends of alignments for creation of pseudosegmentation
-> detailed information about source and target of alignment, which will allow us to run algo. fullfilling rule 4.
and divide atoms into clusters
"""

from collections import defaultdict
from tools import recompute_blocks, load_csl

class alignments:
    
    def __init__(self,input_file):
        self.lines =[]
        self.sequences = {}
        self.breakpoints = defaultdict(lambda: defaultdict(lambda: 0))
        self.ordered_seqs = {}
        index = 0
        with open(input_file, 'r') as f:
            for line in f:
                loaded_line = alignment(index)
                index = index+1
                seq_info = loaded_line.load_psl_line(line)
                breaks = loaded_line.breakpoints()
                
                sequence1_name = seq_info[0]
                sequence1_len = seq_info[1]
                sequence2_name = seq_info[2]
                sequence2_len = seq_info[3]
                self.lines.append(loaded_line)
                self.sequences[sequence1_name] = sequence1_len
                self.sequences[sequence2_name] = sequence2_len
                self.add_breakpoints(sequence1_name, breaks[0])
                self.add_breakpoints(sequence2_name, breaks[1])
                
    def get_sequences(self):
        return self.sequences.keys()
    
    def get_sequence_data(self, seq):
        return self.sequences[seq], self.breakpoints[seq]
    
    #counting number of breakpoint occurences at given position through dict
    def add_breakpoints(self, seq, breakpoints):
        self.breakpoints[seq][breakpoints[0]] = self.breakpoints[seq][breakpoints[0]] + 1
        self.breakpoints[seq][breakpoints[1]] = self.breakpoints[seq][breakpoints[1]] + 1

    # order sequences by start, stop, this will allow us to connect pseudoatoms with relevant alignments in effective way (single pass)
    def order_sequences(self):
        self.ordered_seqs = {}
        for seq in self.sequences:
            self.ordered_seqs[seq] = []
        for i, line in enumerate(self.lines):
            q1 = line.q1
            q2 = line.q2
            q1_seq = q1.sequence
            q1_key = q1.gen_key(i)
            self.ordered_seqs[q1_seq].append(q1_key)
            q2_seq = q2.sequence
            q2_key = q2.gen_key(i)
            self.ordered_seqs[q2_seq].append(q2_key)
        for seq in self.sequences:
            self.ordered_seqs[seq] = sorted(self.ordered_seqs[seq])
        #for seq in self.ordered_seqs.keys():
            
    def get_ordered_alignments(self, seq):
        if seq not in self.ordered_seqs.keys():
            self.order_sequences()
        return sorted(self.ordered_seqs[seq])
            
    def link_atom_to_alignment(self, alignment_index, atom_index, alignment_source):   
        self.lines[alignment_index].link_atom(atom_index, alignment_source)
        
    def unlink_atom(self, alignment_index, atom_index, alignment_source):
        return self.lines[alignment_index].unlink_atom(atom_index, alignment_source)
                
    def atoms_to_check(self, alignment_index, alignment_source):
        return self.lines[alignment_index].atoms_to_check(alignment_source)
    
    def get_alignment(self, alignment_index):
        return self.lines[alignment_index]
                
                
class alignment:
    
    def __init__(self, index):
        self.q1 = None
        self.q2 = None
        self.q_atoms = set()
        self.t_atoms = set()
        self.index = index

        
        
    def load_psl_line(self,line):
        data = line.split()
        sequence1 = data[9]
        sequence2 = data[13]
        self.block_sizes = load_csl(data[18])
        self.blocks = int(data[17])
        q_size = int(data[10])
        t_size = int(data[14])
        q_block_starts = load_csl(data[19])
        t_block_starts = load_csl(data[20])
        strand = data[8]
        if strand == '-':
            self.rotated = True
        else:
            self.rotated = False
        q_blocks = recompute_blocks(q_block_starts, self.block_sizes, q_size, rotated = self.rotated)
        t_blocks = recompute_blocks(t_block_starts, self.block_sizes, t_size, rotated = False)
        
        self.q1 = query(sequence1,int(data[11]),int(data[12]), q_blocks, s_type = 'query')
        self.q2 = query(sequence2,int(data[15]),int(data[16]), t_blocks, s_type = 'target')
        self.q1.target=self.q2
        self.q2.target=self.q1
        #return sequence1_name,length,sequence2_name,length
        return (sequence1, q_size, sequence2, t_size)
    
    
    def __str__(self):
        r = f"alignment of target {self.q1} and  {self.q2} on strand {self.strand}"
        return r


    def breakpoints(self):
        return(self.q1.breakpoints(),self.q2.breakpoints())
    
    def link_atom(self, atom_index, alignment_source):
        if alignment_source == 'query':
            self.q_atoms.add(atom_index)
        elif alignment_source == 'target':
            self.t_atoms.add(atom_index)
            
    def unlink_atom(self, atom_index, alignment_source):
        if alignment_source == 'query':
            self.q_atoms.remove(atom_index)
        elif alignment_source == 'target':
            self.t_atoms.remove(atom_index)
        
    def atoms_to_check(self, alignment_source):
        if alignment_source == 'query':
            return self.t_atoms
        elif alignment_source == 'target':
            return self.q_atoms
    
    def get_query(self, alignment_source):
        if alignment_source == 'query':
            return self.q1
        elif alignment_source == 'target':
            return self.q2
            

    


class query:
    def __init__(self,sequence,start,end, blocks, s_type):
        self.start=start
        self.end=end
        self.sequence = sequence
        self.s_type = s_type
        self.blocks = blocks
        
        
    def breakpoints(self):
        return (self.start,self.end)
    
    
    def __str__(self):
        pass
        #return f"sequence {self.sequence} start {self.start} end {self.end}"
        
    def gen_key(self, index):
        return (self.start, self.end, index, self.s_type)






