import math
import collections
import click
import queue
from bisect import bisect_left,bisect_right
from  tools import waste_penalty, penalty_add, new_waste_penalty, amount_of_regions_penalty, alignment_penalty, zero_penalty, max_penalty, add_wastefield


'''
This package of the program is ment to perform following steps:
    1, load informations about alignments (from .psl) with help of separate package
    2, based on those alignments, create pseudosegmentation - i.e. segmentation fullfiling first three rules
    3, store created meta-results for further processing
'''


class Pseudosegmentation:


    def __init__(self, sequence_name, sequence_length, breakpoints, atom_length, border_length):
        self.sequence_name = sequence_name
        self.sequence_length = sequence_length
        breakpoints[0] = 1
        breakpoints[sequence_length] = 1
        self.breakpoints_counts = breakpoints
        self.breakpoints = set(breakpoints.keys())
        self.breakpoints_list = sorted(list(self.breakpoints))
        self.atom_length = atom_length
        self.border_length = border_length
        '''
        self.penalty = [0] * (sequence_length + 1)
        self.atom_start = [0] * (sequence_length + 1)
        self.region_type = [0] *(sequence_length + 1)
        '''
        self.penalty = {}
        self.atom_start = {}
        self.region_type = {}

        self.previous = [0] * (sequence_length + 1)
        self.extendable_start = [-1] * (sequence_length + 1)
        #previous and atom start should be more or less equal, and as so, interchangable



    '''
    S is sequence for which we are computing atomization
    Penalty is field of penalties, achieved in each position, penalty[i] being lowest penalty
     we can achieve on subfield [0,i]
    alpha_ends is ordered array of positions of alignment ends
    d is length of atom end (core in middle, ends on sides)
    L is minimal length of atom
    '''



    def subsequence(self, i, j):
        border = self.border_length
        # first, check if we can fit atom lenghtwise
        if j - i < self.atom_length:
            return (waste_penalty(i, j), 1)  # waste of len j-i
        # if so, we check if there are no breakpoints inside atom core
        core_start = i + border
        core_end = j - border
        c_s_i = bisect_right(self.breakpoints_list, core_start)
        c_e_i = bisect_left(self.breakpoints_list, core_end)
        if c_e_i - c_s_i > 0:
            return (waste_penalty(i, j), 1)
        c_s_i = c_s_i - 1
        subsq_penalty = zero_penalty()
        # if not, we can fit in atom, and we compute penalty for breakpoints outside of core
        while(c_s_i >= 0) and (self.breakpoints_list[c_s_i] > i):
            bp = self.breakpoints_list[c_s_i]
            count = self.breakpoints_counts[bp]
            subsq_penalty = penalty_add(subsq_penalty, alignment_penalty(i, bp, count))
            c_s_i = c_s_i - 1
        while (c_e_i < len(self.breakpoints_list)) and (self.breakpoints_list[c_e_i] < j):
            bp = self.breakpoints_list[c_e_i]
            count = self.breakpoints_counts[bp]
            subsq_penalty = penalty_add(subsq_penalty, alignment_penalty(j, bp, count))
            c_e_i = c_e_i + 1
        return (penalty_add(subsq_penalty, amount_of_regions_penalty(1)), 0)


    '''
    simple dynamic programing, starting from position 0, program is trying to find optimal solution for given position as addition to
     presolved subfield of position of lower index. for given position i, it tries every position j, j<i.
     score for i is then computed as best score of j (alredy computed), plus addition to score from sequence [j,i] - which is either waste or atom.

    It may seem there is problem with expansion, if position j is end of waste [c,j] , and [i,j] is also waste, we would like to have them as one waste region, not two,
    but [c.j] should give better score
    '''


    def super_slow_algorithm(self):
        self.region_type[0] = 1
        self.atom_start[0] = 0
        self.penalty[0] = amount_of_regions_penalty(1)
        for i in range(1, self.sequence_length + 1):
            if i % 1000 == 0:
                print(f'solving for index {i}')
            step_penalty = max_penalty()  # lowest penalty at position i
            for j in range(0, i):
                subsq_penalty, subsq_type = self.subsequence(j, i)
                j_penalty = penalty_add(self.penalty[j], subsq_penalty)  # todo if best subsq is atom, and at pos j ends previous atom, add waste of len 0 (penalty for num of waste regions)

                if j_penalty < step_penalty:
                    step_penalty = j_penalty
                    best_index = j
                    best_type = subsq_type

            self.region_type[i] = best_type
            self.atom_start[i] = best_index
            self.penalty[i] = step_penalty



    def restore_sequence(self):
        atoms = []
        wastes = []
        index = self.sequence_length
        while(index > 0):
            prev = self.atom_start[index]
            if self.region_type[index] == 0:
                atoms.append((prev,index))
            else:
                wastes.append((prev,index))
            index = prev
        return (self.sequence_name, sorted(atoms), sorted(wastes), self.penalty[self.sequence_length])



    '''
    greedy algorithm, goes trough each position c and put end of waste region there.
    there are two options, either the length of waste is 0, or >0.
    if 0, it means atom ends there so we try to expand atom ending at c-1, or if no atom can end at c-1, we try to create atom of minimal length  -- this tricks also beggining
    (has to do with ends of alingment, in first case getting deeper into atom, in second we maybe can create atom at C and not at c-1 due to shift - ends od alignment popping out of atom core)
    if >0 we just take best score from c-1 and add 1 waste to it (just penalty for len of waste regions, amount of waste regions isn't changing)
    otherwise we add penalty for number of waste regions.

    we need to store if there is posible atom at that position, penalty of position, starting position of atom/waste ending at our position
    also keeping list of alignment ends that are not in core yet, and nearest one that's in core might come handy.


    if we expand atom => we need to recompute penalty for breakpoints inside atom edges (one side), no new waste, no new region
    if we create new atom => we will compute atom penalty (for breakpoints from both sides ) and add penalty for new region
    no atom created => we add penalty for 1 waste, no new region
    '''
    def sequential_algorithm(self):
        self.atom_start[0] = 0
        self.region_type[0] = 1  # 1 means long waste ending, 0 atom ending -> in case of 0 we can extend atom
        self.penalty[0] = amount_of_regions_penalty(1)
        self.extendable_start[0] = -1
        for pos in range(1,self.atom_length):
            self.sa_extend_waste(pos)
        for pos in range(self.atom_length, self.sequence_length + 1):
            if pos % 1000 == 0:
                print(f'solving for index {pos}')
            #for each position, we can for sure extend waste (either from 0 to 1 or existing long waste region)
            self.sa_extend_waste(pos)
            # score if |w| = 0
            #now we can try to extend atom, and compare its score (first compare score if its lesser or EQUAL, we make changes)

            #find first distant BP
            core_end = pos - self.border_length
            c_e_i = bisect_left(self.breakpoints_list, core_end)
            bp = self.breakpoints_list[c_e_i - 1]
            for sub_index in range(bp-self.border_length,bp+self.border_length+1):
                if sub_index < 0:
                    continue
                subsq_penalty, subsq_type = self.subsequence(sub_index, pos)
                total_penalty = penalty_add(self.penalty[sub_index], subsq_penalty)  # todo if best subsq is atom, and at pos j ends previous atom, add waste of len 0 (penalty for num of waste regions)

                if total_penalty < self.penalty[pos]:
                    self.region_type[pos] = subsq_type
                    self.atom_start[pos] = sub_index
                    self.penalty[pos] = total_penalty


    '''
    if previous field is end of atom, we are extending waste with length 0 to length 1
    otherwise extends waste with start
    '''
    def sa_extend_waste(self, i):
        self.region_type[i] = 1
        self.penalty[i] = add_wastefield(self.penalty[i - 1])
        if self.region_type[i-1] == 0:
            self.atom_start[i] = i - 1
        else:
            self.atom_start[i] = self.atom_start[i - 1]


    def forced_atom(self, start, end):
        atom_penalty, sub_type = self.subsequence(start, end)
        if sub_type == 0:
            self.extendable_start[end] = start
        else:
            self.extendable_start[end] = -1


    def sa_evaluate_atom(self, start, end):
        atom_penalty, sub_type = self.subsequence(start, end)
        if sub_type == 0:
            final_penalty = penalty_add(atom_penalty, self.penalty[start])
            if final_penalty <= self.penalty[end]:
                self.penalty[end] = final_penalty
                self.region_type[end] = 0
                self.atom_start[end] = start
                return 0
        return 1

    '''
    We don't have to know scoring achievable for every index, it's enough to know scores on indexes surrounding breakpoints.
    psuedocode:
        keep in var:
            indexes to process -> iterable
            current and previous index
            breakpoints -> linked list / ordered set
            last_bp - last breakpoint observer
            last bp tail - by tail we mean breakpoints closer than d_1
            previous_bp and its tail -> once index shifts significantly (at least L - d_1 from last BP) store last_bp and last_bp tail
               into previous, to easily access them for atom penalty computation
        1, get ordered set of all indexes to process -> every breakpoint and its proximity
        2,go through indexes to process:
            updat last_bp and last_bp_tail if new BP was encountered,
            and/or previous_bp + previous_bp_tail if shift was significant
            if index is closer to last_bp then L - d_1, we cannot fit atom

    '''
    def neighbourhood_search(self):
        border = self.border_length
        neighbourhoods = set()
        for bp in self.breakpoints:
            hood = {x for x in range(bp-border,bp+border+1)}
            neighbourhoods = neighbourhoods.union(hood)
        neighbourhoods.add(self.sequence_length)
        #rather than veryfiing if each hood is > 0 and < seq length, we remove outliers later
        for x in range(-border,0):
            neighbourhoods.discard(x)
            neighbourhoods.discard(self.sequence_length - x)
        near = queue.Queue()
        far = queue.Queue()
        #pre solve for index 0 manually
        for x in range(0-border,0+border+1):
            self.penalty[x] = max_penalty()
        self.atom_start[0] = 0
        self.region_type[0] = 1  # 1 means long waste ending, 0 atom ending -> in case of 0 we can extend atom
        self.penalty[0] = amount_of_regions_penalty(1)
        far.put(0)
        previous_index = 0
        neighbourhoods.discard(0)
        #solve for others
        for index in sorted(neighbourhoods):
            #update lists of BP before and after core
            if index in self.breakpoints:
                near.put(index)
            while (not near.empty()) and near.queue[0] < index - border:
                far.put(near.get())
            while (not far.empty()) and far.queue[0] < far.queue[-1] - border:
                far.get()
            # extend waste from previous solved index (multiple wastes)
            self.penalty[index] = penalty_add(self.penalty[previous_index], waste_penalty(previous_index, index))
            self.region_type[index] = 1
            if self.region_type[previous_index] == 1:
                self.atom_start[index] = self.atom_start[previous_index]
            else:
                self.atom_start[index] = previous_index
            # try placing atoms starting around far.queue[-1]
            bp = far.queue[-1]
            for sub_index in range(bp-border,bp+border+1):
                self.hood_atom(near, far, sub_index, index)
            #shift previous_index
            previous_index = index

    def hood_atom(self, near, far, sub_index, index):
        if index - sub_index < self.atom_length:
            return
        subsq_penalty = zero_penalty()

        for bp in far.queue:
            if bp > sub_index:
                count = self.breakpoints_counts[bp]
                subsq_penalty = penalty_add(subsq_penalty, alignment_penalty(sub_index, bp, count))

        for bp in near.queue:
            count = self.breakpoints_counts[bp]
            subsq_penalty = penalty_add(subsq_penalty, alignment_penalty(index, bp, count))

        total_penalty = penalty_add(subsq_penalty, amount_of_regions_penalty(1))
        total_penalty = penalty_add(total_penalty, self.penalty[sub_index])

        if total_penalty < self.penalty[index]:
            self.region_type[index] = 0
            self.atom_start[index] = sub_index
            self.penalty[index] = total_penalty
