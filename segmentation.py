#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 13 21:08:31 2021

@author: user
"""
import math
from collections import defaultdict
import random as rand
from tools import (update_alignment_indexes, find_block_coordinates,
                   previous_coordinate, translate_coordinate, opposite_query,
                   min_coordinate, max_coordinate, next_coordinate, rotate_sequence,
                   get_blocks, print_d, recompute_coordinates
                   )


class Atomization:

    def __init__(self, alignments, min_atom_length, d1, d2):
        self.alignments = alignments
        self.sequences = alignments.sequences
        self.atoms = []
        self.meta_atom_data = defaultdict(dict)

        self.min_atom_length = min_atom_length
        self.d2_const = d2
        self.d1_const = d1
        self.min_projection_length = min_atom_length - 2 * d2
        self.min_overlap_relevant = min_atom_length - d2
        self.min_inside_relevant = self.min_projection_length
        self.current_index = 0
        # queues
        self.to_process = set()
        self.to_split = set()
        self.free_indexes = set()
        # get alignments sorted (by alignment start,end) per sequence, this is needed to get to find out which alignment contains which atom

    def add_atoms_for_sequence(self, sequence, pseudoatoms):
        for atom in pseudoatoms:
            new_atom = Atom(atom, sequence, self.current_index)
            self.atoms.append(new_atom)
            key = new_atom.get_metadata()
            self.meta_atom_data[sequence][key] = self.current_index
            self.to_process.add((self.current_index, -1, -1))
            self.current_index = self.current_index + 1

        self.match_alignments(sequence)

        # create pseudoatoms for given sequence
        # add those atoms into alignment refs (keep information about wheter its source or target -- it might save some time)
        # link refs for alignments to those atoms

    def match_alignments(self, sequence):
        # order atoms for sequence
        ordered_atoms = sorted(self.meta_atom_data[sequence])
        ordered_alignments = self.alignments.get_ordered_alignments(sequence)
        start_index, end_index = 0, 0
        for atom_meta in ordered_atoms:
            atom_index = self.meta_atom_data[sequence][atom_meta]
            atom = self.atoms[atom_index]
            start_index, end_index = update_alignment_indexes(start_index, end_index, atom, ordered_alignments,
                                                              edge=self.d1_const)
            print_d(f"indexes {atom.start}, {atom.end},{start_index}, {end_index}")
            # for ordered_alignments_index in range(start_index, end_index):
            for ordered_alignments_index in range(0, len(ordered_alignments)):

                alignment_meta = ordered_alignments[ordered_alignments_index]
                alignment_index = alignment_meta[2]
                alignment_source = alignment_meta[3]
                aligned_segment = self.alignments.get_alignment(alignment_index).get_query(alignment_source)
                if (aligned_segment.start <= (atom.start + self.d1_const)) and (
                        aligned_segment.end >= (atom.end - self.d1_const)):
                    print_d(
                        f"linking {atom.start}, {atom.end},{alignment_index}, {alignment_source}, {aligned_segment.start}, {aligned_segment.end}")
                    flag = self.link_alignment(atom_index, alignment_index, alignment_source)
                    if flag == -1:
                        self.delete_atom(atom_index, notify=False)
                        break

    def link_alignment(self, atom_index, alignment_index, alignment_source):
        atom = self.atoms[atom_index]
        alignment = self.alignments.lines[alignment_index]
        coordinates = find_block_coordinates(atom.start, atom.end, alignment, alignment_source)
        if coordinates == -1:
            print_d(f"coordinates {coordinates} alignment {alignment_index}, {atom_index}")
            print_d(f'{atom.start}, {atom.end}, {alignment_source}')
            return -1

        self.atoms[atom_index].add_alignment(alignment_index, alignment_source, coordinates)
        projection = self.get_projection(atom_index, alignment_index, alignment_source)
        self.alignments.link_atom_to_alignment(alignment_index, atom_index, alignment_source)
        if projection[0][1] - projection[0][0] < self.min_projection_length:
            print_d(f"projection {projection}")
            return -1
        return 0

    def process_pseudo_atoms(self):
        # for each atom
        print_d(f"to process {len(self.to_process)} ")
        while (len(self.to_process) + len(self.to_split)) > 0:
            if len(self.to_process) > 0:
                atom_index, alignment_index, alignment_source = self.to_process.pop()
                soft_splitting = 0
            else:
                min_split = min(self.to_split)
                self.to_split.remove(min_split)
                soft_splitting, atom_index, alignment_index, alignment_source = min_split
            if atom_index == 1:
                pass
            atom = self.atoms[atom_index]
            if atom is None:
                continue
            all_alignments = {k for k in atom.alignments}
            tasks = {(alignment_index, alignment_source)}
            if alignment_index == -1:
                tasks = all_alignments
            atom_deleted = False
            notify = False
            while len(tasks) > 0 and not (atom_deleted):
                atom_trimmed = False
                task = tasks.pop()
                alignment_index, alignment_source = task
                alignment = self.alignments.get_alignment(alignment_index)

                trimming = True
                projection_meta = self.get_projection(atom_index, alignment_index, alignment_source)
                if projection_meta == -1 or projection_meta[0][1] - projection_meta[0][0] < self.min_projection_length:
                    self.delete_atom(atom_index)
                    atom_deleted = True
                    continue

                relevant_atoms = self.get_relevant_atoms(projection_meta, alignment, alignment_source)
                number_of_relevant = len(relevant_atoms)

                while trimming and not (atom_deleted):
                    trimming = False
                    if len(relevant_atoms) == 0:
                        self.delete_atom(atom_index)
                        atom_deleted = True
                        continue
                    if len(relevant_atoms) > 1:
                        relevant_atoms = self.order_relevant(relevant_atoms)
                    trimmed = self.trimm_atom(relevant_atoms, alignment, atom, alignment_source)
                    if trimmed == -1:
                        atom_deleted = True
                        continue
                    if trimmed == 1:
                        atom_trimmed = True
                        notify = True
                        projection_meta = self.get_projection(atom_index, alignment_index, alignment_source)
                        if projection_meta == -1 or projection_meta[0][1] - projection_meta[0][
                            0] < self.min_projection_length:
                            self.delete_atom(atom_index)
                            atom_deleted = True
                            continue
                        relevant_atoms = self.get_relevant_atoms(projection_meta, alignment, alignment_source)
                        if len(relevant_atoms) < number_of_relevant:
                            number_of_relevant = len(relevant_atoms)
                            trimming = True
                            continue
                    if number_of_relevant > 1:
                        split_flag = self.split_atom(atom, relevant_atoms, alignment, alignment_source,
                                                     hard_flag=soft_splitting)
                        if split_flag > 2:
                            self.to_split.add((split_flag, atom_index, alignment_index, alignment_source))
                        elif split_flag == 0:
                            atom_deleted = True
                            continue
                        elif split_flag == 2:
                            atom_trimmed = True
                            notify = True
                        elif split_flag == -1:
                            atom_deleted = True
                            continue
                if atom_trimmed:
                    tasks = tasks.union(all_alignments)
            if notify:
                for k in all_alignments:
                    self.notify_atoms(k[0], k[1])

    def notify_atoms(self, alignment_index, alignment_source):
        alignment_target = opposite_query(alignment_source)
        to_notify = self.alignments.atoms_to_check(alignment_index, alignment_source)
        for atom_index in to_notify:
            self.to_process.add((atom_index, alignment_index, alignment_target))

    def trimm_atom(self, relevant_atoms, alignment, atom, atom_query, trimming_data=-1):
        if trimming_data == -1:
            trimming_data = self.compute_trimming(relevant_atoms, alignment, atom_query, atom)

        min_start = trimming_data[0][0]
        max_end = trimming_data[0][1]
        if max_end - min_start < self.min_atom_length:
            self.delete_atom(atom.index)
            return -1

        if min_start > atom.start or max_end < atom.end:
            atom.start = min_start
            atom.end = max_end
            flag = recompute_coordinates(atom, self)
            if flag == -1:
                self.delete_atom(atom.index)
                return -1
            return 1

        return 0
        # trimming
        # compare start of projection with start of first relevant and end with end of last

        # based on coordinates of start / end of relevant atom, find coordinate relevant to cutting

        # initial cutting coordinates picked from mix of where atom needs to be cut to pass rule 4, and where relevant atom will expect atom

    def compute_trimming(self, relevant_atoms, alignment, alignment_source, atom):
        projection_query = opposite_query(alignment_source)
        first_relevant = self.atoms[relevant_atoms[0]]
        last_relevant = self.atoms[relevant_atoms[-1]]
        f_r_start = first_relevant.start
        f_r_s_coordinate = first_relevant.get_alignment_meta(alignment.index, projection_query)[0][0]
        l_r_end = last_relevant.end
        l_r_e_coordinate = last_relevant.get_alignment_meta(alignment.index, projection_query)[0][1]

        # find where should start crop itself + where is relevant atom expecting it
        start_cutting_coordinate = f_r_s_coordinate
        if f_r_s_coordinate > min_coordinate():
            prev_coordinate = previous_coordinate(f_r_s_coordinate, alignment, projection_query)

            while (translate_coordinate(prev_coordinate, alignment, projection_query) > f_r_start - self.d2_const):
                start_cutting_coordinate = prev_coordinate
                if (prev_coordinate > min_coordinate()):
                    print_d(f"previous coordinate from {prev_coordinate}, min_coordinate {min_coordinate()}")
                    prev_coordinate = previous_coordinate(prev_coordinate, alignment, projection_query)
                else:
                    break

        end_cutting_coordinate = l_r_e_coordinate
        if l_r_e_coordinate < max_coordinate(alignment, projection_query):
            n_coordinate = next_coordinate(l_r_e_coordinate, alignment, projection_query)

            while (translate_coordinate(n_coordinate, alignment, projection_query) < l_r_end + self.d2_const):
                end_cutting_coordinate = n_coordinate
                if (n_coordinate < max_coordinate(alignment, projection_query)):
                    n_coordinate = next_coordinate(n_coordinate, alignment, projection_query)
                else:
                    break

        if alignment.rotated:
            start_cutting_coordinate, end_cutting_coordinate = rotate_sequence(
                (start_cutting_coordinate, end_cutting_coordinate), alignment, projection_query)
            f_r_s_coordinate, l_r_e_coordinate = rotate_sequence((f_r_s_coordinate, l_r_e_coordinate), alignment,
                                                                 projection_query)

        if start_cutting_coordinate == min_coordinate():
            first_pos_by_projection = atom.start
        else:
            prev = previous_coordinate(start_cutting_coordinate, alignment, alignment_source)
            first_pos_by_projection = translate_coordinate(prev, alignment, alignment_source) + 1
        first_pos_by_relevant = translate_coordinate(f_r_s_coordinate, alignment, alignment_source) - self.d2_const
        earliest_pos = max(atom.start, first_pos_by_projection, first_pos_by_relevant)

        if end_cutting_coordinate == max_coordinate(alignment, alignment_source):
            last_pos_by_projection = atom.end
        else:
            later = next_coordinate(end_cutting_coordinate, alignment, alignment_source)
            last_pos_by_projection = translate_coordinate(later, alignment, alignment_source) - 1
        last_pos_by_relevant = translate_coordinate(l_r_e_coordinate, alignment, alignment_source) + self.d2_const
        latest_pos = min(atom.end, last_pos_by_projection, last_pos_by_relevant)

        return (earliest_pos, latest_pos), (f_r_s_coordinate, l_r_e_coordinate), (
        start_cutting_coordinate, end_cutting_coordinate)

    def order_relevant(self, relevant_atoms):
        to_order = []
        for atom_index in relevant_atoms:
            to_order.append((self.atoms[atom_index].start, atom_index))
        return [k[1] for k in sorted(to_order)]

    def split_atom(self, atom, relevant_atoms, alignment, atom_query, hard_flag = 0):
        flag, splitting = self.compute_splitting(atom, relevant_atoms, alignment, atom_query)
        if flag == -1:
            self.delete_atom(atom.index)
            return -1
        if flag == 2:
            t_flag = self.trimm_atom(relevant_atoms, alignment, atom, atom_query, trimming_data=(splitting,))
            if t_flag == -1:
                return -1
            return 2
        sequence = atom.sequence
        if flag > hard_flag:
            return flag
        for segment in splitting:
            if len(self.free_indexes) > 0:
                new_index = self.free_indexes.pop()
            else:
                new_index = self.current_index
                self.current_index = self.current_index + 1
                self.atoms.append(None)
            new_atom = Atom(segment, sequence, new_index)
            self.atoms[new_index] = new_atom
            self.to_process.add((new_index, -1, -1))
            for meta_key in atom.alignments:
                alignment_index, alignment_source = meta_key
                flag = self.link_alignment(new_index, alignment_index, alignment_source)
                if flag == -1:
                    self.delete_atom(new_index, notify=False)
                    break
        self.delete_atom(atom.index)
        return 0

    def get_projection(self, atom_index, alignment_index, alignment_source):
        # find first block with end > atom_start, and last with start < atom_end
        print_d(f"atom index {atom_index}")
        atom = self.atoms[atom_index]
        alignment = self.alignments.lines[alignment_index]
        source_coordinates = atom.get_alignment_meta(alignment_index, alignment_source)[0]
        alignment_target = opposite_query(alignment_source)

        rotated = alignment.rotated
        blocks = get_blocks(alignment, alignment_target)

        if rotated:
            target_coordinates = rotate_sequence(source_coordinates, alignment, alignment_source)
        else:
            target_coordinates = source_coordinates
        (s_b, s_i), (e_b, e_i) = target_coordinates
        projection_start = blocks[s_b][0] + s_i
        projection_end = blocks[e_b][0] + e_i
        return (projection_start, projection_end), target_coordinates, source_coordinates, rotated

    def delete_atom(self, atom_index, notify=True):
        # notify "interested" atoms and remove from alignments

        atom = self.atoms[atom_index]
        for meta_key in atom.alignments:
            alignment_index, alignment_source = meta_key
            self.alignments.unlink_atom(alignment_index, atom_index, alignment_source)

            if notify:
                self.notify_atoms(alignment_index, alignment_source)

        # remove from atoms
        self.atoms[atom_index] = None
        # self.free_indexes.add(atom_index)

    def get_relevant_atoms(self, projection_meta, alignment, alignment_source):
        if alignment_source == 'query':
            atoms_pool = alignment.t_atoms
        else:
            atoms_pool = alignment.q_atoms
        relevant = []
        projection = projection_meta[0]
        start = projection[0]
        end = projection[1]
        projection_len = end - start
        for atom_index in atoms_pool:
            atom = self.atoms[atom_index]
            if (atom.start <= start) and (atom.end >= end):
                if (projection_len >= self.min_inside_relevant):
                    relevant.append(atom_index)
                continue

            if (atom.start >= start) and (atom.end <= end):
                relevant.append(atom_index)
                continue
            if min(atom.end, end) - max(atom.start, start) >= self.min_overlap_relevant:
                relevant.append(atom_index)

        return relevant


    def find_split_pos(self, computed_trimming, i, alignment, alignment_source):
        meta_one = computed_trimming[i]
        meta_two = computed_trimming[i + 1]
        first_end = translate_coordinate(meta_one[1][1], alignment, alignment_source)
        last_start = translate_coordinate(meta_two[1][0], alignment, alignment_source)
        middle = first_end + (last_start - first_end) / 2
        if middle % 1 == 0:
            pos = [int(middle)]
        else:
            m = math.floor(middle)
            pos = [int(m), int(m+1)]

        if pos[0] >= meta_one[0][1]:
            pos = [meta_one[0][1]]
        if pos[-1] <= meta_two[0][0]:
            pos = [meta_two[0][0]]
        return pos


    def pick_pos(self, first, second, s_p):
        if first + second == 2:
            return s_p
        elif first:
            return [s_p[0]]
        else:
            return [s_p[-1]]


    def compute_splitting(self, atom, relevant_atoms, alignment, atom_query):
        computed_trimming = []
        for r_atom in relevant_atoms:
            r_a_trimming = self.compute_trimming([r_atom], alignment, atom_query, atom)
            if r_a_trimming[0][1] - r_a_trimming[0][0] >= self.min_atom_length:
                computed_trimming.append(r_a_trimming)
        if alignment.rotated:
            computed_trimming = computed_trimming[::-1]

        # there is no possible splitting at all, as all trimming segments are shorter than min atom length
        if len(computed_trimming) == 0:
            return -1, []

        # soft splitting -> if there is a gap between two, where we can place cut safely
        overlaps = []
        cutout = []
        m_a_l = self.min_atom_length
        for i in range(len(computed_trimming) - 1):
            meta_one = computed_trimming[i]
            meta_two = computed_trimming[i + 1]
            overlap = meta_one[0][1] - meta_two[0][0]

            # chceck if there is no overlap between end cut of one atom and start cut of another
            if overlap <= 0:
                cutout.append((meta_one[0][1], meta_two[0][0]))
            else:
                f_r_s = meta_one[0][0]
                if i > 0:
                    f_r_s = max(computed_trimming[i-1][0][1], f_r_s)
                f_r_e = meta_two[0][0]
                s_r_s = meta_one[0][1]
                s_r_e = meta_two[0][1]
                if i < len(computed_trimming) - 2:
                    s_r_e = min(s_r_e, computed_trimming[i+2][0][0])
                s_p = self.find_split_pos(computed_trimming, i, alignment, atom_query)

                if f_r_e - f_r_s >= m_a_l and s_r_e - s_r_s >= m_a_l:
                    overlaps.append((10,overlap,i,s_p))
                else:
                    rule_1_f = s_p[0] - f_r_s >= m_a_l and s_r_e - s_p[0] >= m_a_l
                    rule_1_s = rule_1_f
                    rule_2_f = s_p[0] - meta_one[0][0] >= m_a_l and meta_two[0][1] - s_p[0] >= m_a_l
                    rule_2_s = rule_2_f
                    if len(s_p) > 1:
                        rule_1_s = s_p[-1] - f_r_s >= m_a_l and s_r_e - s_p[-1] >= m_a_l
                        rule_2_s = s_p[-1] - meta_one[0][0] >= m_a_l and meta_two[0][1] - s_p[-1] >= m_a_l

                    if rule_1_f or rule_1_s:
                        overlaps.append((11, overlap, i, self.pick_pos(rule_1_f, rule_1_s, s_p)))
                    elif rule_2_f or rule_2_s:
                        overlaps.append((12, overlap, i, self.pick_pos(rule_2_f, rule_2_s, s_p)))
                    else:
                        save_both = []
                        save_one = []
                        for k in range(meta_two[0][0],meta_one[0][1] + 1):
                            rule_3_f = k - meta_one[0][0] > m_a_l
                            rule_3_s = meta_two[0][1] - k > m_a_l
                            if rule_3_f and rule_3_s:
                                save_both.append(k)
                            elif rule_3_f or rule_3_s:
                                save_one.append(k)
                        if len(save_both) > 0:
                            overlaps.append((13, overlap, i, save_both))
                        else:
                            overlaps.append((14, overlap, i, [meta_two[0][0], meta_one[0][1]]))


        # soft split
        if len(cutout) > 0:
            splitting = []
            start = atom.start # start = max(atom.start, computed_trimming[0][0][0])
            for c in cutout:
                end = c[0]
                splitting.append((start, end))
                start = c[1]
            end = min(atom.end, computed_trimming[-1][0][1])
            splitting.append((start, end))
            return 0, splitting

        # atom can be trimmed instead of hard splitting, this happens if first or last relevant atom trimming segment is too short or only one remains
        if computed_trimming[0][0][0] > atom.start or computed_trimming[-1][0][1] < atom.start:
            return 2, [computed_trimming[0][0][0], computed_trimming[-1][0][1]]

        # pick least destructive hard split
        if len(overlaps) == 0:
            print_d("No splitting position was at least somehow valid.")
            return -1, []
        best = sorted(overlaps)[0]
        flag, overlap, index, s_p = best
        index = 0
        if len(s_p) > 1:
            index = rand.randint(0, len(s_p) - 1)
        return flag, [(atom.start, s_p[index]), (s_p[index], atom.end)]


class Atom:

    def __init__(self, boundaries, sequence, index):
        self.start = boundaries[0]
        self.end = boundaries[1]
        self.index = index
        self.sequence = sequence
        self.alignments = {}
        self.coordinates = defaultdict()
        self.output_id = None
        self.output_class = None
        self.output_strand = None
        # keep relevant atoms for projection through each alignment and if it pass rule 4

    def add_alignment(self, alignment_index, query, coordinates):
        # find out if atom is aligned to source or
        # store additional meta infromations as atoms coordinates in given alignment and status?
        self.alignments[(alignment_index, query)] = (coordinates, -1)

    def get_alignment_meta(self, alignment_index, query):
        return self.alignments[(alignment_index, query)]

    def set_alignment_meta(self, alignment_index, query, coordinates=None, status=None):
        new_coordinates, new_status = self.get_alignment_meta(alignment_index, query)
        if coordinates != None:
            new_coordinates = coordinates
        if status != None:
            new_status = status
        self.alignments[(alignment_index, query)] = (new_coordinates, new_status)

    def get_metadata(self):
        return (self.start, self.end)

    def output_atom(self):
        delim = '\t'
        return f"{self.sequence}{delim}{self.output_id}{delim}{self.output_class}{delim}{self.output_strand}{delim}{self.start}{delim}{self.end}"
