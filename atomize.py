from load_psl import alignments
from pseudosegmentation import Pseudosegmentation
from collections import Counter
from segmentation import Atomization
from tools import generate_output, print_d
import click
import math

    ## Full run #########
    
    



'''
    module grouping rest of the modules, allowing for creation of full segmentation, 
    from provided alignment data, full atomization is produced.
    on the output, we recieve list of atoms together with their split into groups.
'''

@click.command()
@click.argument('input_file')
@click.option('-l','--atom_length', 'min_atom_length', default=100, help='Minimal atom length')
@click.option('-d1','--edge_slack', 'd1_const', default=10, help='Max depth of alignment end inside atom')
@click.option('-d1','--match_slack', 'd2_const', default=15, help='Max distance for atom projection end difference')
@click.option('-o','--output', default=None, help='Output file name')
@click.option('-v','--validate', default=False)
def main(input_file, min_atom_length, d1_const, d2_const, output, validate):
    limit = math.floor(min_atom_length/3)
    if d1_const > limit or d2_const > limit:
        raise ValueError("Constants d1 and d2 for slack should be much smaller than minimal atom length.")
    if output is None:
        separator = '.'
        fields = input_file.split(separator)
        if 'psl' in fields:
            i = fields.index('psl')
            output = separator.join(fields[:i])
        else:
            output = separator.join(fields[:-1])
        output = f"{output}.atoms"
    
    loaded_input = alignments(input_file)
    atomization = Atomization(loaded_input, min_atom_length, d1_const, d2_const)
    seqs = loaded_input.get_sequences()
    for seq in seqs:
        seq_data = loaded_input.get_sequence_data(seq)
        ps = Pseudosegmentation(seq, seq_data[0], seq_data[1], min_atom_length, d1_const)
        ps.neighbourhood_search()
        pa = ps.restore_sequence()
        atomization.add_atoms_for_sequence(pa[0], pa[1])
        
        
    atomization.process_pseudo_atoms()
    
    if validate:
        for atom in atomization.atoms:
            if atom is None:
                continue
            print_d(f"Atoms start {atom.start} and end {atom.end}")
            all_alignments = {k for k in atom.alignments}
            print_d(f"Atom in {len(all_alignments)} alignments")
            for metadata in all_alignments:
                alignment_index, alignment_source = metadata
                alignment = atomization.alignments.get_alignment(alignment_index)
                projection_meta = atomization.get_projection(atom.index, alignment_index, alignment_source)
                relevant_atoms = atomization.get_relevant_atoms(projection_meta, alignment, alignment_source)
                if len(relevant_atoms) != 1:
                    raise Exception("Somethin went wrong, atom is not matching exactly one atom through alignment")
                    print_d(f"Not 1 relevant {relevant_atoms}")
                p_s,p_e = projection_meta[0][0],projection_meta[0][1]
                r_atom = atomization.atoms[relevant_atoms[0]]
                r_s,r_e = r_atom.start, r_atom.end
                print_d(f"in alignment {alignment_index} projection {p_s,p_e} matching atom {r_s},{r_e} diff {r_s - p_s},{r_e - p_e}")
                if abs(r_s - p_s) > atomization.d2_const or abs(r_e - p_e) > atomization.d2_const:
                    raise Exception("Something went wrong, end of projection is too far away from end of atom")
                    
    
    generate_output(atomization, output)
    
    

if __name__ == '__main__':
    main()
