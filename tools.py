#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 10 13:56:06 2021

@author: user
"""
import math


global print_debug_flag
print_debug_flag = False

def penalty_add(a, b):
    #print(a)
    l = len(a)
    assert l == len(b) == 3 , "Penalty a isn't same length as penalty b"      
    new_p = [a[i] + b[i] for i in range(l)]
    return tuple(new_p)

def add_wastefield(a, k = 1):
    b = waste_field_penalty(field=k)
    return penalty_add(a, b)

# 1 penalty for each base in  1 penalty for waste field, 1/N penalty for number of waste regions. 1/N^2 for alignment end in atom
def waste_penalty(i, j):
    waste = abs(j - i)
    return (waste, 0, 0)

def waste_field_penalty(field = 1):
    return (field, 0, 0)

def new_waste_penalty(i, j):
    waste_p = waste_penalty(i, j)
    region_p = amount_of_regions_penalty(1)
    return penalty_add(waste_p, region_p)

# 1/len for each waste region - not depending on len
def amount_of_regions_penalty(k):
    return (0, k, 0)


#
def alignment_penalty(i, k, count = 1):
    penalty = (k - i) ** 2
    penalty = count * penalty
    #print(penalty)
    return (0, 0, penalty)


def zero_penalty():
    return (0, 0, 0)


def max_penalty():
    return (math.inf, math.inf, math.inf)


def print_d(message):
    if print_debug_flag:
        print(message)


def update_alignment_indexes(start_index, end_index, atom, ordered_alignments, edge = 0):
    start = atom.start + edge
    end = atom.end - edge
    first_ending = get_indexed_alignment_end(ordered_alignments, start_index)
    last_starting = get_indexed_alignment_start(ordered_alignments, end_index)
    
    while(start >= first_ending):
        start_index = start_index + 1
        first_ending = get_indexed_alignment_end(ordered_alignments, start_index)
        
    while(end > last_starting):
        end_index = end_index + 1
        last_starting = get_indexed_alignment_start(ordered_alignments, end_index)
        
    return start_index, end_index


def get_indexed_alignment_start(ordered_alignments, index):
    if index < len(ordered_alignments):
        return ordered_alignments[index][0]
    return math.inf
    
    
def get_indexed_alignment_end(ordered_alignments, index):
    if index < len(ordered_alignments):
        return ordered_alignments[index][1]
    return math.inf


'''
we want to store blocks in a format that tells us their start and end index on positive strand, ordered
for blocks on positive strand, we just put that information together, 
for blocks on negative strand, we compute that infromation based on q_size, block_start and block_size
'''
def recompute_blocks(block_starts, block_sizes, q_size, rotated=False):
    blocks = []
    if rotated:
        for i, block_start in enumerate(block_starts):
            end = q_size - block_start
            start = end - block_sizes[i]
            blocks.append((start,end))
        blocks = blocks[::-1]
    else:
        for i,start in enumerate(block_starts):
            blocks.append((start,start+block_sizes[i]))
    return blocks

def load_csl(csl):
    listed = csl.strip(',').split(',')
    to_int = [int(x) for x in listed]
    return to_int

#add handling of cases with no projection
def find_block_coordinates(start, end, alignment, alignment_source):
    block_num = alignment.blocks
    blocks = get_blocks(alignment, alignment_source)
    s_b = 0
    while blocks[s_b][1] <= start:
        s_b = s_b + 1
        if s_b >= block_num:
            return -1
    e_b = block_num - 1
    while blocks[e_b][0] >= end:
        e_b = e_b - 1
        if e_b < 0:
            return -1
    if s_b > e_b:
        return -1
    #find index in each block, first field of min/max query is for when atom start/end lies in that block
    #second is for when block is fully inside atom
    s_i = max(start - blocks[s_b][0], 0)
    e_i = min(end - blocks[e_b][0], blocks[e_b][1] - blocks[e_b][0])
    return ((s_b, s_i),(e_b, e_i))


def recompute_coordinates(atom, atomization):
    for meta_key in atom.alignments:
        alignment_index, alignment_source = meta_key
        alignment = atomization.alignments.get_alignment(alignment_index)
        new_coordinates = find_block_coordinates(atom.start, atom.end, alignment, alignment_source)
        if new_coordinates == -1:
            print_d(f"coordinates {new_coordinates} alignment {alignment_index}, {atom}")
            print_d(f'{atom.start}, {atom.end}, {alignment_source}')
            return -1
        projection = atomization.get_projection(atom.index, alignment_index, alignment_source)
        if projection[0][1] - projection[0][0] < atomization.min_projection_length:
            print_d(f"projection {projection}")
            return -1
        atom.set_alignment_meta(alignment_index, alignment_source, coordinates = new_coordinates)
    return 0
        

#rotate coordinates for alignments on negative strand
def rotate_sequence(source_coordinates, alignment, alignment_source):
    start_coordinate, end_coordinate = source_coordinates

    r_end_coordinate = rotate_coordinate(start_coordinate, alignment, alignment_source)
    r_start_coordinate = rotate_coordinate(end_coordinate, alignment, alignment_source)
    
    return (r_start_coordinate,r_end_coordinate)

def rotate_coordinate(coordinate, alignment, alignment_source):
    if coordinate == -1:
        return -1
    if alignment_source == 'query':
        q = alignment.q1
    else:
        q = alignment.q2
    block_num = alignment.blocks
    blocks = q.blocks
    block_c,index_c = coordinate
    rotated_block_c = block_num - 1 - block_c
    rotated_index_c = blocks[block_c][1] - blocks[block_c][0] - index_c
    
    return rotated_block_c, rotated_index_c

def opposite_query(alignment_source):
    if alignment_source == 'query':
        return 'target'
    if alignment_source == 'target':
        return 'query'
    raise ValueError(f'Either "query" or "target" is valid refference of alignment source got {alignment_source}')

def get_blocks(alignment, alignment_source):
    if alignment_source == 'query':
        q = alignment.q1
    else:
        q = alignment.q2
    return q.blocks

def translate_coordinate(coordinate, alignment, source):
    if source == 'query':
        blocks = alignment.q1.blocks
    else:
        blocks = alignment.q2.blocks
    print_d('----------------')
    print_d(f"blocks {blocks}")
    print_d(coordinate)
    print_d(f"block start {blocks[coordinate[0]]}")
    try:
        print_d(f"offset {coordinate[1]}")
    except:
        pass
    return blocks[coordinate[0]][0] + coordinate[1]

def previous_coordinate(coordinate, alignment, alignment_source, skip_block_end = True):
    block, offset = coordinate
    if offset > 0:
        return block, offset -1
    if block == 0:
        return -1
    block = block -1
    blocks = get_blocks(alignment, alignment_source)
    offset = blocks[block][1] - blocks[block][0] - int(skip_block_end)
    
    return block, offset

def next_coordinate(coordinate, alignment, alignment_source, skip_block_start = True):
    block, offset = coordinate
    blocks = get_blocks(alignment, alignment_source)
    max_offset = blocks[block][1] - blocks[block][0]
    
    if offset < max_offset:
        offset = offset + 1
    elif block == len(blocks) - 1:
        return -1
    else:
        block = block + 1
        offset = 0 + int(skip_block_start)
    
    return block, offset
    
def max_coordinate(alignment, alignment_source):
    blocks = get_blocks(alignment, alignment_source)
    block_coordinate = len(blocks) -1
    offset = blocks[-1][1] - blocks[-1][0]
    return block_coordinate, offset

def get_output_indexing(Atomization):
    free_index = 0
    to_sort = []
    for atom in Atomization.atoms:
        if atom is None:
            continue
        to_sort.append((atom.sequence,atom.start,atom.index))
    indexing = [i[2] for i in sorted(to_sort)]
    for i in indexing:
        Atomization.atoms[i].output_id = free_index
        free_index += 1
    return indexing

def generate_output(Atomization, output_file_name):
    indexing = get_output_indexing(Atomization)
    classify(Atomization, indexing)
    with open(output_file_name, 'w') as f:
        for i in indexing:
            atom = Atomization.atoms[i]
            f.write(f"{atom.output_atom()}\n")
        

def classify(Atomization, indexing):
    free_class = 1
    for index in indexing:
        atom = Atomization.atoms[index]
        processed, total = set_class(Atomization, atom, free_class, 1)
        free_class +=1
        if total < 0:
            for i in processed:
                atom = Atomization.atoms[index]
                atom.output_strand = atom.output_strand * -1
 
def set_class(Atomization, atom, free_class, strand):
    if atom.output_class is not None:
        return [], 0
    atom.output_class = free_class
    atom.output_strand = strand
    all_alignments = {k for k in atom.alignments}
    total = strand
    listed = [atom.index]
    for metadata in all_alignments:
        alignment_index, alignment_source = metadata
        alignment = Atomization.alignments.get_alignment(alignment_index)
        projection_meta = Atomization.get_projection(atom.index, alignment_index, alignment_source)
        relevant_atoms = Atomization.get_relevant_atoms(projection_meta, alignment, alignment_source)
        if len(relevant_atoms) != 1:
            raise ValueError('Exactly one relevant atom excpected')
        r_atom = Atomization.atoms[relevant_atoms[0]]
        new_strand = strand
        if alignment.rotated:
            new_strand = strand * -1
        processed,balance = set_class(Atomization, r_atom, free_class, new_strand)
        total = total + balance
        listed.extend(processed)
    return listed, total
    
def min_coordinate():
    return 0,0