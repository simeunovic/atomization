#!/usr/bin/python

import sys

atoms = []

input_file = sys.argv[1]
output_file = sys.argv[2]
filter_out = int(sys.argv[3])

with open(input_file) as f:
    for l in f:
        if l[0] == '#':
            continue
        line = l.strip('\n').split('\t')
        atom_len = int(line[4]) - int(line[3])
        if atom_len < filter_out:
            continue
        strand = 1
        if line[6] == '-':
            strand = -1

        key = (line[0],int(line[3]),int(line[4]),int(line[8].strip('id=')),strand)
        atoms.append(key)

atoms.sort()
for i in range(0,len(atoms) - 1):
    j = i+1
    while(j<len(atoms)) and (atoms[i][2] > atoms[j][1]) and (atoms[i][0] == atoms[j][0]):
        gtfo = atoms.pop(j)
        print(gtfo)
        
for i in range(0,len(atoms) - 1):
    a1 = atoms[i]
    a2 = atoms[i+1]
    if a1[0] == a2[0] and a1[2] > a2[1]:
        print(f"overlaping {a1} and {a2}")


with open(output_file, 'w') as f:
    for i,a in enumerate(atoms):
        f.write(f"{a[0]}\t{i}\t{a[3]}\t{a[4]}\t{a[1]}\t{a[2]}\n")
