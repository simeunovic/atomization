#! /usr/bin/python3
#ad all atoms
#iterate trough atom classses,
#for each class check if it's collapsible - followed, preceeded always by same type.
#if yes, collapse
#print output


class Atom:
    def __init__(self,strand,id,type,orientation,start,stop,prev):
        self.strand=strand
        self.id=id
        self.type=type
        self.orientation=orientation
        self.start=int(start)
        self.end=int(stop)
        self.prev=prev
        self.next=None
    def __str__(self):
        return self.strand+"\t"+self.id+"\t"+self.type+"\t"+self.orientation+"\t"+str(self.start)+"\t"+str(self.end)+""

def collapsible(atom1):
    #chceck if len fits, strand fits,
    if atom1.next == None:
        return 0
    atom2 = atom1.next
    type = atom1.type
    o1 = atom1.orientation
    o2 = atom2.orientation
    type2 = atom2.type
    types1 = byType[type]
    if type in checkedTypes:
        return 0
    if len(byType[type])!=len(byType[type2]):
        return 0
    for at in types1:
        if at.orientation == o1:
           if at.next == None:
               return 0
           at2 = at.next
           if at2.orientation != o2:
               return 0
           if at.strand != at2.strand:
               return 0
           if at2.type != type2:
               return 0
           if at2.start - at.end > maxLen:
               return 0
        else:
            if at.prev == None:
                return 0
            at2 = at.prev
            if at2.orientation == o2:
                return 0
            if at2.strand != at.strand:
                return 0
            if at2.type != type2:
                return 0
            if at.start - at2.end > maxLen:
                return 0
    
    return 1


def collapse(atom1):
    atom2 = atom1.next
    type = atom1.type
    o1 = atom1.orientation
    o2 = atom2.orientation
    type2 = atom2.type
    types1 = byType[type]
    for at in types1:
        if at.orientation == o1:
            at2 = at.next
            if at2.next != None:
                at.next = at2.next
                at3 = at2.next
                at3.prev = at
            else:
                at.next = None
            at2.next = None
            at2.prev = None
            at.end = at2.end
        else:
            at2 = at.prev
            if at2.prev != None:
                at3 = at2.prev
                at3.next = at
                at.prev = at3
            else:
                at.prev = None
                first=at
            at2.next = None
            at2.prev = None
            at.start=at2.start


import sys
inputfile=""
maxLen = 1000
atoms = []
byType = {}
checkedTypes = []

# open a file for reading
for i in sys.argv[1:]:
    splitted = i.split("=")
    if splitted[0] == "-i":
        inputfile=splitted[1]
    elif splitted[0] == "-l":
        maxLen=int(splitted[1])
    else:
         print("collapese-atoms.py -i=<inputfile> -l=<max length between atoms, def=1000>")
         sys.exit(2)
prev = None
first = None

with open(inputfile) as csvfile:
    # iterate over lines of the input file
    for line in csvfile:
        line = line.strip("\n")
        columns = line.split("\t")
        a = Atom(columns[0],columns[1],columns[2],columns[3],columns[4],columns[5],prev)
        if prev != None:
            prev.next = a
        prev=a
        if first == None:
            first = a
        atoms.append(a)
        list = byType.get(columns[2],[])
        list.append(a)
        byType[columns[2]]=list


collapsed = 1
while collapsed:
    collapsed = 0
    atom = first
    while atom != None:
        while collapsible(atom):
            collapse(atom)
            collapsed=1
        else:
            checkedTypes.append(atom.type)
        atom=atom.next

atom=first
while atom!= None:
    print(atom.__str__())
    atom=atom.next

