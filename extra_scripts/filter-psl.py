#! /usr/bin/python3
import sys
inputfile=""
minMatch = 0.8
minLen = 100
# open a file for reading
for i in sys.argv[1:]:
    splitted = i.split("=")
    if splitted[0] == "-i":
        inputfile=splitted[1]
    elif splitted[0] == "-l":
        minLen=int(splitted[1])
    elif splitted[0] == "-id":
        minMatch=float(splitted[1])/100
    else:
         print("filter-psl.py -i=<inputfile> -l=<min length, def=100> -id=<min id,def = 80 %>")
         sys.exit(2)

with open(inputfile) as csvfile:
    # iterate over lines of the input file
    for line in csvfile:
        line = line.strip("\n")
        if line[0] == '#':
            continue
        columns = line.split("\t")
        matches = int(columns[0])+int(columns[2])
        all_bases = matches + int(columns[1])
        if (all_bases > minLen) and ((matches/all_bases) >= minMatch):
            print(line)

