# Script for running lastz on all pairs of sequences in input fasta file.
# Written by Michal Burger.
# Changed by David Simeunovic to use last
#for multiple fasta files create $inp.last in tmp, then for each i in $tmp/i.last, run lastal inside cycle lastal $tmp/$i $inp
for inp in $@; do
	tmp=`mktemp -d`
	lastdb $tmp/$inp.last $inp
	# > $tmp/$inp.last #last isn't creating .last file, just .last.*
	# >$inp.psl
	#for i in $tmp/*.last; do
	#	lastal $i $inp | maf-convert psl > $i.psl
	#	cat $i.psl >> $inp.psl
	#done
	lastal -E1e-10 -m100 $tmp/$inp.last $inp | maf-convert psl > $inp.2.psl
	rm -r $tmp
done
